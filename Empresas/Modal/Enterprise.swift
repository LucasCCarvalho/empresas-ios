//
//  Enterprise.swift
//  Empresas
//
//  Created by Lucas Carvalho on 08/12/18.
//  Copyright © 2018 Lucas Carvalho. All rights reserved.
//

import Foundation

struct Enterprise: Mappable {
    
    var enterprises: [EnterpriseItens]
    
    init(mapper: Mapper) {
        self.enterprises = mapper.keyPath("enterprises")
    }
}
