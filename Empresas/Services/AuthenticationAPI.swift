//
//  AuthenticationAPI.swift
//  Empresas
//
//  Created by Lucas Carvalho on 08/12/18.
//  Copyright © 2018 Lucas Carvalho. All rights reserved.
//

import Foundation

public class AuthenticationAPI: APIRequest {
        
    override init(method: API.HTTPMethod, path: String, parameters: [String : Any]?, urlParameters: [String : Any]?, cacheOption: API.CacheOption, completion: ResponseBlock<Any>?) {
        super.init(method: method, path: path, parameters: parameters, urlParameters: urlParameters, cacheOption: cacheOption, completion: completion)
        
        self.baseURL = URL(string: "http://empresas.ioasys.com.br/api/")!
    }
    
    @discardableResult
    static func loginWith(email: String, password: String, callback: ResponseBlock<User>?) -> AuthenticationAPI {
        
        let request = AuthenticationAPI(method: .post, path: "v1/users/auth/sign_in", parameters: ["email": email, "password": password], urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            if let error = error {
                callback?(nil, error, cache)
            } else if let response = response as? [String: Any] {
                let user = User(dictionary: response)
                callback!(user, nil, cache)
            }
        }
        request.shouldSaveInCache = false
        request.makeRequest()
        
        return request
    }
}
