//
//  DetailsViewController.swift
//  Empresas
//
//  Created by Lucas Carvalho on 09/12/18.
//  Copyright © 2018 Lucas Carvalho. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    @IBOutlet weak var txaDescription: UITextView!
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblPhoto: UILabel!
    
    var valueDrescription: String!
    var valuePhoto: UIImage!
    var nameShortEnterprise: String!
    var nameEnterprise: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblPhoto.text = nameShortEnterprise
        txaDescription.text = valueDrescription
        imvPhoto.image = valuePhoto
        createVisual()
    }
    
    func createVisual(){
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.barTintColor = UIColor(red: 222/255.0, green: 71/255.0, blue: 114/255.0, alpha: 1.0)
        navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.title = nameEnterprise
    }
}
