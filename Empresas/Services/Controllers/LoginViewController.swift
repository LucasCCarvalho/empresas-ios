//
//  LoginViewController.swift
//  Empresas
//
//  Created by Lucas Carvalho on 08/12/18.
//  Copyright © 2018 Lucas Carvalho. All rights reserved.
//

import UIKit

extension UITextField {
    
    func setBottomLine(borderColor: UIColor) {
        
        self.borderStyle = UITextField.BorderStyle.none
        self.backgroundColor = UIColor.clear
        
        let borderLine = UIView()
        let height = 1.0
        borderLine.frame = CGRect(x: 0, y: Double(self.frame.height) - height, width: Double(self.frame.width), height: height)
        
        borderLine.backgroundColor = borderColor
        self.addSubview(borderLine)
    }
    
}

class LoginViewController: UIViewController {

    @IBOutlet weak var txvEmail: UITextField!
    @IBOutlet weak var txvPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        createVisual()
        closeKeyBoard()
        
        txvEmail.text = "testeapple@ioasys.com.br"
        txvPassword.text = "12341234"
    }
    
    @IBAction func btnLoginAction(_ sender: Any) {
        if(!validateEmail(email: self.txvEmail.text!)){
            let mensagem = "Enter a valid email to continue!"
            let alerta = UIAlertController(title: "Ops!", message: mensagem, preferredStyle: .alert)
            alerta.addAction(UIAlertAction(title: "Confirmar", style: .default, handler: nil))
            self.present(alerta, animated: true, completion: nil)
        } else if (txvPassword.text == "") {
            let mensagem = "Enter a password to continue!"
            let alerta = UIAlertController(title: "Ops!", message: mensagem, preferredStyle: .alert)
            alerta.addAction(UIAlertAction(title: "Confirmar", style: .default, handler: nil))
            self.present(alerta, animated: true, completion: nil)
        } else {
            AuthenticationAPI.loginWith(email: txvEmail.text!, password: txvPassword.text!) {
                (response, error, cache) in
                if(error == nil) {
                    self.performSegue(withIdentifier: "segueLogin", sender: self)
                }
            }
        }
    }
    
    
    func createVisual(){
        txvEmail.leftViewMode = UITextField.ViewMode.always
        let imageViewEmail = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 30))
        imageViewEmail.contentMode = UIView.ContentMode.left
        let imageEmail = UIImage(named: "icEmail.png")
        imageViewEmail.image = imageEmail
        txvEmail.leftView = imageViewEmail
        
        txvPassword.leftViewMode = UITextField.ViewMode.always
        let imageViewLock = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 30))
        imageViewLock.contentMode = UIView.ContentMode.left
        let imageLock = UIImage(named: "icCadeado.png")
        imageViewLock.image = imageLock
        txvPassword.leftView = imageViewLock
        
        let lineColor = UIColor.lightGray
        self.txvEmail.setBottomLine(borderColor: lineColor)
        self.txvPassword.setBottomLine(borderColor: lineColor)
        self.btnLogin.layer.cornerRadius = 5

    }
    
    

    

    
    
    /*
     Métodos criados para permitir que o teclado seja fechado caso o usuário clique fora do componente.
     */
    
    func closeKeyBoard(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        self.view.isUserInteractionEnabled = true
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    /*
     Método criado para permitir a validação do email.
     */
    
    func validateEmail(email:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: email)
        
    }
}
