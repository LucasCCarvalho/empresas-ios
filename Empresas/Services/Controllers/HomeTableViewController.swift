//
//  HomeTableViewController.swift
//  Empresas
//
//  Created by Lucas Carvalho on 08/12/18.
//  Copyright © 2018 Lucas Carvalho. All rights reserved.
//

import UIKit

extension String {
    var forSorting: String {
        let simple = folding(options: [.diacriticInsensitive, .widthInsensitive, .caseInsensitive], locale: nil)
        let nonAlphaNumeric = CharacterSet.alphanumerics.inverted
        return simple.components(separatedBy: nonAlphaNumeric).joined(separator: "")
    }
}

class HomeTableViewController: UITableViewController, UISearchBarDelegate, UITabBarControllerDelegate {
    @IBOutlet weak var btnSearch: UIBarButtonItem!
    @IBOutlet var tbvEnterprises: UITableView!
    
    lazy var searchBar: UISearchBar = UISearchBar()
    var searchController: UISearchController = UISearchController()
    var itens: [EnterpriseItens] = []
    var itensFiltro: [EnterpriseItens] = []
    var searchBarText: String!
    var valueDrescription: String!
    var valuePhoto: UIImage!
    var nameShortEnterprise: String!
    var nameEnterprise: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createVisual()
        

    }
    
    func createVisual(){
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.barTintColor = UIColor(red: 222/255.0, green: 71/255.0, blue: 114/255.0, alpha: 1.0)
        navigationController?.navigationBar.shadowImage = UIImage()
        let logoNav = UIImage(named: "logoNav.png")
        let imageViewNav = UIImageView(image: logoNav)
        self.navigationItem.titleView = imageViewNav
        
        self.tableView.tableFooterView = UIView()
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        searchController = UISearchController(searchResultsController: nil)
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.barTintColor =  UIColor(red: 222/255.0, green: 71/255.0, blue: 114/255.0, alpha: 1.0)
        searchController.searchBar.layer.borderWidth = 1
        searchController.searchBar.layer.borderColor = UIColor(red: 222/255.0, green: 71/255.0, blue: 114/255.0, alpha: 1.0).cgColor
        searchController.searchBar.tintColor = UIColor.white
        searchController.searchBar.placeholder = "Pesquisar"
        searchController.searchBar.keyboardType = UIKeyboardType.asciiCapable
        searchController.searchBar.delegate = self
        searchController.searchBar.text = searchBarText
        present(searchController, animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.dismiss(animated: true) {
            DispatchQueue.main.async {
                let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                let alert = UIAlertController(title: nil, message: "Carregando...", preferredStyle: .alert)
                
                loadingIndicator.hidesWhenStopped = true
                loadingIndicator.style = UIActivityIndicatorView.Style.gray
                loadingIndicator.startAnimating()
                alert.view.addSubview(loadingIndicator)
                self.present(alert, animated: true, completion: nil)
                
                /*
                 Observação sobre o consumo desta API:
                 Entendo perfeitamente que as empresas são tipadas e que eu conseguiria retornar uma lista de empresas por tipo, porém no escopo de nosso projeto em nenhum momento informamos o tipo da empresa, sendo assim para que eu não trabalhe apenas um tipo especifico de empresa em nosso app não utilizarei este parametro do filtro.
                 */
                EnterprisesAPI.listEnterprises(type: "", name: searchBar.text!) { (response, error, cache) in
                    if(error == nil) {
                        self.itens.removeAll()
                        self.itens = (response?.enterprises)!
                        self.tableView.reloadData()
                    }
                    alert.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if(searchBar.text != ""){
            searchBarText = searchBar.text!
        }
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if(itens.count > 0) {
            tableView.backgroundView = nil
            return 1
        } else {
            let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            label.text = "Clique na busca para iniciar."
            label.textColor = UIColor.black
            label.font = UIFont(name: "Open Sans", size: 15)
            label.textAlignment = .center
            tableView.backgroundView = label
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itens.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellEnterprises", for: indexPath) as! EnterpriseCell
        let currentObject: EnterpriseItens = itens[indexPath.row]

        cell.lblName.text = currentObject.enterprise_name
        cell.lblType.text = currentObject.enterprise_type_name
        cell.lblCountry.text = currentObject.country
        
        if(currentObject.photo != "<null>"){
            let url = URL(string: "http://empresas.ioasys.com.br" + currentObject.photo!)
            let data = try? Data(contentsOf: url!)
            cell.lblPhoto.text = ""
            cell.imvPhoto.image = UIImage(data: data!)
        } else {
            cell.lblPhoto.text = String(currentObject.enterprise_name.first!).forSorting.uppercased() + String(currentObject.enterprise_name.last!).forSorting.uppercased()
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! EnterpriseCell
        let currentObject: EnterpriseItens = itens[indexPath.row]
        
        valueDrescription = currentObject.description
        nameEnterprise = currentObject.enterprise_name
        nameShortEnterprise = cell.lblPhoto.text
        valuePhoto = cell.imvPhoto.image
        self.performSegue(withIdentifier: "segueDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueDetail"{
            let detailVC = segue.destination as! DetailsViewController
            detailVC.valueDrescription = self.valueDrescription
            detailVC.valuePhoto = self.valuePhoto
            detailVC.nameEnterprise = self.nameEnterprise
            detailVC.nameShortEnterprise = self.nameShortEnterprise

        }
    }

}
