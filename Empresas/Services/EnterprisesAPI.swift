//
//  EnterprisesAPI.swift
//  Empresas
//
//  Created by Lucas Carvalho on 08/12/18.
//  Copyright © 2018 Lucas Carvalho. All rights reserved.
//

import Foundation

public class EnterprisesAPI: APIRequest {

    override init(method: API.HTTPMethod, path: String, parameters: [String : Any]?, urlParameters: [String : Any]?, cacheOption: API.CacheOption, completion: ResponseBlock<Any>?) {
        super.init(method: method, path: path, parameters: parameters, urlParameters: urlParameters, cacheOption: cacheOption, completion: completion)
        
        self.baseURL = URL(string: "http://empresas.ioasys.com.br/api/")!
    }
    
    @discardableResult
    static func listEnterprises(type: String, name: String, callback: ResponseBlock<Enterprise>?) -> EnterprisesAPI {
        
        let request = EnterprisesAPI(method: .get, path: "v1/enterprises", parameters: nil, urlParameters: ["name": name], cacheOption: .networkOnly) { (response, error, cache) in
            if let error = error {
                callback?(nil, error, cache)
            } else if let response = response as? [String: Any] {
                let user = Enterprise(dictionary: response)
                callback!(user, nil, cache)
            }
        }
        request.shouldSaveInCache = false
        request.makeRequest()
        
        return request
    }
}
