//
//  EnterpriseCell.swift
//  Empresas
//
//  Created by Lucas Carvalho on 09/12/18.
//  Copyright © 2018 Lucas Carvalho. All rights reserved.
//

import UIKit

class EnterpriseCell: UITableViewCell {
    
    @IBOutlet weak var lblPhoto: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var imvPhoto: UIImageView!
}
