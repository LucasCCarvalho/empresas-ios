//
//  User.swift
//  Empresas
//
//  Created by Lucas Carvalho on 08/12/18.
//  Copyright © 2018 Lucas Carvalho. All rights reserved.
//

import Foundation

struct User: Mappable {
    
    var investorName: String
    var email: String
    var city: String
    var country: String
    
    init(mapper: Mapper) {
        self.investorName = mapper.keyPath("investor.investor_name")
        self.email = mapper.keyPath("investor.email")
        self.city = mapper.keyPath("investor.city")
        self.country = mapper.keyPath("investor.country")
    }
}
