//
//  EnterpriseItens.swift
//  Empresas
//
//  Created by Lucas Carvalho on 08/12/18.
//  Copyright © 2018 Lucas Carvalho. All rights reserved.
//

import Foundation

struct EnterpriseItens: Mappable {
    
    var id: Int
    var email_enterprise: String
    var facebook: String
    var twitter: String
    var linkedin: String
    var phone: String
    var own_enterprise: String
    var enterprise_name: String
    var photo: String?
    var description: String
    var city: String
    var country: String
    var value: Int
    var share_price: Int
    var enterprise_type_name: String
    
    init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.email_enterprise = mapper.keyPath("email_enterprise")
        self.facebook = mapper.keyPath("facebook")
        self.twitter = mapper.keyPath("twitter")
        self.linkedin = mapper.keyPath("linkedin")
        self.phone = mapper.keyPath("phone")
        self.own_enterprise = mapper.keyPath("own_enterprise")
        self.enterprise_name = mapper.keyPath("enterprise_name")
        self.photo = mapper.keyPath("photo")
        self.description = mapper.keyPath("description")
        self.city = mapper.keyPath("city")
        self.country = mapper.keyPath("country")
        self.value = mapper.keyPath("value")
        self.share_price = mapper.keyPath("share_price")
        self.enterprise_type_name = mapper.keyPath("enterprise_type.enterprise_type_name")
        
    }
}
